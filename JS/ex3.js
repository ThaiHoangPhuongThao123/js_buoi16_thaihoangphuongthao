function tinhThue() {
var ten = document.getElementById('ten-ex3').value;
var soNguoi = document.getElementById('so-nguoi-ex3').value * 1;
var tongThuNhap = document.getElementById("tong-thu-nhap-ex3").value * 1;
var thuNhapChiuThue = tongThuNhap - 4 - soNguoi * 1.6;
var thueThuNhap = tinhThueSuat(thuNhapChiuThue);
thueThuNhap = thueThuNhap.toFixed(3);
document.getElementById("resuft3").innerText = `Tên cá nhân nộp thuế: ${ten}
Thu nhập chịu thuế: ${thuNhapChiuThue} triệu đồng
Thuế thu nhập cá nhân: ${thueThuNhap} triệu đồng`;
}

function tinhThueSuat(thuNhapChiuThue) {
    if (thuNhapChiuThue <= 60) {
        return thuNhapChiuThue * 0.05;
    } else if (thuNhapChiuThue <= 120) {
        return thuNhapChiuThue * 0.1;
    } else if (thuNhapChiuThue <= 210) {
        return thuNhapChiuThue * 0.15;
    }else if (thuNhapChiuThue <= 384) {
        return thuNhapChiuThue * 0.2;
    }else if (thuNhapChiuThue <= 624) {
        return thuNhapChiuThue * 0.25;
    }else if (thuNhapChiuThue <= 960) {
        return thuNhapChiuThue * 0.3;
    } else {
       return thuNhapChiuThue * 0.35; 
    }
}