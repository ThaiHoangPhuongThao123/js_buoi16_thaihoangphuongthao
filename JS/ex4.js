document.getElementById("soKetNoi").style.display = "none";

function tinhTienCap() {
  var soKenhCaoCap = document.getElementById("so-kenh-ex4").value * 1;
  var hoaDonKhachHang = 0;
  var maKH = document.getElementById("ma-khach-hang-ex4").value;
  var thongTinhKH = ghiThongTin();
  if (thongTinhKH== "B") {
    hoaDonKhachHang = layHoaDonNhaDan(soKenhCaoCap);
  } else if (thongTinhKH == "A") {
    var soKetNoi = document.getElementById("so-ket-noi-ex4").value * 1;
    hoaDonKhachHang = layHoaDonDoanhNghiep(soKenhCaoCap, soKetNoi);
  }

  document.getElementById("resuft4").innerText = `Mã khach hàng: ${maKH}
    Hóa đơn khách hàng: ${hoaDonKhachHang}
    `;
}

function ghiThongTin() {
  if (document.getElementById("loaiKH").value == "B") {
    document.getElementById("soKetNoi").style.display = "none";
    return "B";
  } else if (document.getElementById("loaiKH").value == "A") {
    document.getElementById("soKetNoi").style.display = "block";
    return "A";
  }
}

function layHoaDonNhaDan(soKenhCaoCap) {
  var phiXuLyHoaDon = 4.5;
  var phiDichVuCoBan = 20.5;
  var thueKenhcCaoCap = 7.5;
  return (
    phiXuLyHoaDon * 1 + phiDichVuCoBan * 1 + thueKenhcCaoCap * soKenhCaoCap
  );
}

function layHoaDonDoanhNghiep(soKenhCaoCap, soKetNoi) {
  var phiXuLyHoaDon = 15;
  var phiDichVuCoBan;
  var thueKenhcCaoCap = 50;
  if (soKetNoi < 10) {
    phiDichVuCoBan = soKetNoi * 5;
  } else {
    phiDichVuCoBan = 75 + (soKetNoi - 10) * 5;
  }

  return (
    phiXuLyHoaDon * 1 + phiDichVuCoBan * 1 + thueKenhcCaoCap * soKenhCaoCap
  );
}
